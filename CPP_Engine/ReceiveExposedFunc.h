#pragma once

namespace Engine
{
	class ExposedClass;
	void ReceiveExposedFunc(Engine::ExposedClass* thing);
}
