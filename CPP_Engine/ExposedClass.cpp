#include "ExposedClass.h"
#include <iostream>

namespace Engine
{
	ExposedClass::ExposedClass() :
		cpp_int(0)
	{}

	ExposedClass::~ExposedClass()
	{}

	void ExposedClass::cpp_func()
	{
		std::cout << "Ran cpp_func()" << std::endl;
	}
}
