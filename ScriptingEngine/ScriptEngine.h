#pragma once
#include "DLLExportMacro.h"
#include <vector>

namespace SharpScript
{
	class Script;
	class ScriptEngineImpl;

	class API ScriptEngine
	{
	public:
		ScriptEngine();
		~ScriptEngine();
		void LoadReferenceAssembly(const char* assemblyName);
		Script* LoadScript(const char* scriptFilename, std::vector<const char*>* references = nullptr);
		Script* LoadScriptAssembly(const char* assemblyFilename, std::vector<const char*>* references = nullptr);

	private:
		ScriptEngineImpl* pScriptEngineImpl;
	};
}
