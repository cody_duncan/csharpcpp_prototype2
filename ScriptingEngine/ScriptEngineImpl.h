#pragma once
#include <unordered_map>
#include <vector>
#include <vcclr.h>
#using <System.dll>

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace SharpScript
{
	class Script;

	class ScriptEngineImpl
	{
	public:
		ScriptEngineImpl();
		~ScriptEngineImpl();

		void LoadReferenceAssembly(const char* assemblyName);
		Script* LoadScript(const char* scriptFilename, std::vector<const char*>* references = nullptr);
		Script* LoadScriptAssembly(const char* assemblyFilename, std::vector<const char*>* references = nullptr);

	private:
		std::unordered_map<std::string, Script*> m_scripts;
		gcroot<Dictionary<String^, System::Reflection::Assembly^>^> m_assemblies;
	};
}
