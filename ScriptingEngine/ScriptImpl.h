#pragma once
#include <vector>
#include <vcclr.h>
#using <System.dll>

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;

namespace Engine
{
	class ExposedClass;
}

namespace SharpScript
{
	class ScriptImpl
	{
	public:
		ScriptImpl();
		ScriptImpl(Assembly^ assembly);
		~ScriptImpl();

		bool CallStaticFunction(const char* functionName);
		bool CallStaticFunction(const char* functionName, Engine::ExposedClass* e);

	private:
		gcroot<Assembly^> compiledAssembly;
	};
}