#include "ScriptImpl.h"
#include <vector>
#include <vcclr.h>
#using <System.dll>
#using <mscorlib.dll>
#using <System.Core.dll>
#using <CLI_Wrapper.dll>

// .Net System Namespaces
using namespace System;
using namespace System::Runtime::InteropServices;
using namespace Microsoft::CSharp;
using namespace System::CodeDom::Compiler;
using namespace System::Collections::Generic;
using namespace System::Reflection;
using namespace System::Text;
using namespace System::Threading::Tasks;
using namespace System::Linq::Expressions;

namespace SharpScript
{
	ScriptImpl::ScriptImpl() :
		compiledAssembly(nullptr)
	{}

	ScriptImpl::ScriptImpl(Assembly^ assembly) :
		compiledAssembly(assembly)
	{}

	ScriptImpl::~ScriptImpl()
	{}

	bool ScriptImpl::CallStaticFunction(const char* functionName)
	{
		array<Type^>^ types = this->compiledAssembly->GetTypes();
		for each(Type^ t in types)
		{
			MethodInfo^ method = t->GetMethod(gcnew String(functionName), BindingFlags::Static | BindingFlags::NonPublic | BindingFlags::Public | BindingFlags::FlattenHierarchy);
			if (method != nullptr)
			{
				method->Invoke(nullptr, nullptr);
				return true;
			}
		}
		return false;
	}

	bool ScriptImpl::CallStaticFunction(const char* functionName, Engine::ExposedClass* e)
	{
		CLI::ExposedClass^ e2 = gcnew CLI::ExposedClass(e);
		array<CLI::ExposedClass^>^ parameterArr = gcnew array<CLI::ExposedClass^>(1);
		parameterArr[0] = e2;

		array<Type^>^ types = this->compiledAssembly->GetTypes();
		for each(Type^ t in types)
		{
			MethodInfo^ method = t->GetMethod(gcnew System::String(functionName), BindingFlags::Static | BindingFlags::NonPublic | BindingFlags::Public | BindingFlags::FlattenHierarchy);
			
			//Notes: create a delegate
			//These are up to three orders of magnitude faster than calling MethodInfo::Invoke.
			//Can be used to memoize function calls.
			{
				//Delegate^ d = Delegate::CreateDelegate(t, method);
			}

			if (method != nullptr)
			{
				method->Invoke(nullptr, parameterArr); // MethodInfo::Invoke() is slow?, or at least has lookup cost.
				return true;
			}
		}
		return false;
	}
}