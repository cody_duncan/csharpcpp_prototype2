#include "Script.h"
#include "ScriptImpl.h"


namespace SharpScript
{
	Script::Script() :
		scriptImpl(new ScriptImpl())
	{}

	Script::Script(ScriptImpl* scriptImpl) :
		scriptImpl(scriptImpl)
	{}

	Script::~Script()
	{
		delete scriptImpl;
	}

	void Script::ReloadScript(ScriptImpl* newScriptImpl)
	{
		delete scriptImpl;
		scriptImpl = newScriptImpl;
	}

	void Script::CallStaticFunction(const char* functionName)
	{
		scriptImpl->CallStaticFunction(functionName);
	}
	
	void Script::CallStaticFunction(const char* functionName, Engine::ExposedClass* e)
	{
		scriptImpl->CallStaticFunction(functionName, e);
	}
	
}