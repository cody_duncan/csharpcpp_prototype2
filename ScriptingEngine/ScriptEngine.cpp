#include "ScriptEngine.h"
#include "ScriptEngineImpl.h"

namespace SharpScript
{
	ScriptEngine::ScriptEngine() :
		pScriptEngineImpl(new ScriptEngineImpl)
	{}

	ScriptEngine::~ScriptEngine()
	{
		delete pScriptEngineImpl;
	}

	void ScriptEngine::LoadReferenceAssembly(const char* assemblyName)
	{
		pScriptEngineImpl->LoadReferenceAssembly(assemblyName);
	}

	Script* ScriptEngine::LoadScript(const char* scriptFilename, std::vector<const char*>* references)
	{
		return pScriptEngineImpl->LoadScript(scriptFilename, references);
	}

	Script* ScriptEngine::LoadScriptAssembly(const char* assemblyFilename, std::vector<const char*>* references)
	{
		return pScriptEngineImpl->LoadScriptAssembly(assemblyFilename, references);
	}
}
