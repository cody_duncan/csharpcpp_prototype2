#include <memory>
#include <vector>
#include "DLLExportMacro.h"

namespace Engine
{
	class ExposedClass;
}
#pragma make_public (Engine::ExposedClass)


namespace SharpScript
{
	class ScriptImpl;

	class API Script
	{
	public:
		Script();
		Script(ScriptImpl*);
		~Script();
		void ReloadScript(ScriptImpl* newScriptImpl);
		void CallStaticFunction(const char* functionName);
		void CallStaticFunction(const char* functionName, Engine::ExposedClass* e);
	private:
		ScriptImpl* scriptImpl;
	};
}