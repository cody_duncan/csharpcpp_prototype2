#include "Script.h"
#include "ScriptEngine.h"
#include "ExposedClass.h"
#include "EntryPoint.h"
#include "Stopwatch.h"
#include <iostream>

int Run()
{
	SharpScript::ScriptEngine engine;
	engine.LoadReferenceAssembly("CSharp_Project.dll");
	SharpScript::Script* script = engine.LoadScriptAssembly("CSharp_Project.dll");

	script->CallStaticFunction("TestFunction");

	Engine::ExposedClass e;
	script->CallStaticFunction("TestExposed", &e);
	
	system("PAUSE");

	return 0;
}
