/* 
	Stopwatch class
	Author: Cody Duncan
*/

#pragma once
#include <Windows.h>
#include <chrono>

/// <summary>
/// Stopwatch for timing execution of code.
/// </summary>
class Stopwatch
{
public:
	using Clock     = std::chrono::high_resolution_clock;
	using TimePoint = std::chrono::time_point<Clock>;
	using Duration  = std::chrono::nanoseconds;

	Stopwatch(bool SingleThread = true);

	void Start();
	void Stop();

	double ElapsedTimeSeconds() const;
	double ElapsedTimeMilliSeconds() const;
	Duration ElapsedTime() const;

private:

	TimePoint m_startCount;
	Duration  m_elapsedTime;

private:
	// *** Ban copy ***
	Stopwatch& operator=(const Stopwatch&) = delete;
	Stopwatch(const Stopwatch&) = delete;
};
