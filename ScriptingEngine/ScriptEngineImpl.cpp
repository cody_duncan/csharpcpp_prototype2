#include "ScriptEngineImpl.h"
#include "ScriptImpl.h"
#include "Script.h"

#include <vector>
#include <vcclr.h>

#using <System.dll>
#using <System.Core.dll>
#using <mscorlib.dll>
#using <CLI_Wrapper.dll>

// .Net System Namespaces
using namespace System;
using namespace System::Diagnostics;
using namespace System::Runtime::InteropServices;
using namespace Microsoft::CSharp;
using namespace System::CodeDom::Compiler;
using namespace System::Collections::Generic;
using namespace System::Reflection;
using namespace System::Text;
using namespace System::Threading::Tasks;
using namespace System::Linq::Expressions;

namespace SharpScript
{
	ScriptEngineImpl::ScriptEngineImpl()
	{
		m_assemblies = gcnew Dictionary<System::String^, System::Reflection::Assembly^>();
	}

	ScriptEngineImpl::~ScriptEngineImpl()
	{
		m_assemblies->Clear();
		m_assemblies = nullptr;
	}


	void ScriptEngineImpl::LoadReferenceAssembly(const char* assemblyName)
	{
		Assembly^ newAssembly = Assembly::LoadFrom(gcnew String(assemblyName));
		m_assemblies->Add(gcnew String(assemblyName), newAssembly);
	}



	Script* ScriptEngineImpl::LoadScript(const char* _scriptFilename, std::vector<const char*>* references)
	{
		#ifdef _DEBUG
			bool isDebug = true;
		#else
			bool isDebug = false;
		#endif
		
		String^ scriptFilename = gcnew String(_scriptFilename);
		array<String^>^ scriptNames = { scriptFilename };

		// Compile the script file
		CSharpCodeProvider^ codeProvider = gcnew CSharpCodeProvider();
		CompilerParameters^ parameters = gcnew CompilerParameters();
		parameters->IncludeDebugInformation = isDebug;
		parameters->GenerateInMemory = true;
		CompilerResults^ results = codeProvider->CompileAssemblyFromFile(parameters, scriptNames);

		//check for errors
		if (results->Errors->HasErrors)
		{
			StringBuilder^ errors = gcnew StringBuilder("Compiler Errors :\r\n");
			for each(CompilerError^ error in results->Errors)
			{
				errors->AppendFormat("Line {0},{1}\t: {2}\n",
					error->Line, 
					error->Column, 
					error->ErrorText
				);
			}
			Debug::WriteLine("Compilation Error occurred in {0}.", scriptFilename);
			Debug::WriteLine(errors);
			return nullptr;
		}

		// Create the Script
		ScriptImpl* scriptImple = new ScriptImpl(results->CompiledAssembly);
		Script* script = new Script(scriptImple);

		if (m_scripts.find(_scriptFilename) != m_scripts.end()) // new script
		{
			
			script = new Script(scriptImple);
			m_scripts.insert(std::make_pair(_scriptFilename, script));
		}
		else // existing script reloaded
		{
			script = m_scripts[_scriptFilename];
			script->ReloadScript(scriptImple);
		}

		return script;
	}



	Script* ScriptEngineImpl::LoadScriptAssembly(const char* _assemblyFilename, std::vector<const char*>* references)
	{
		Dictionary<String^, System::Reflection::Assembly^>^ m_assemblies = this->m_assemblies; // unwrap m_assemblies

		String^ assemblyFilename = gcnew String(_assemblyFilename);
		Assembly^ assembly = nullptr;

		if (!m_assemblies->ContainsKey(assemblyFilename)) // new assembly
		{
			assembly = Assembly::LoadFrom(assemblyFilename); // Loads from a separate context.
			m_assemblies->Add(assemblyFilename, assembly);
		}
		else // get existing assembly
		{
			assembly = m_assemblies[assemblyFilename];
		}

		// Create the Script
		ScriptImpl* scriptImple = new ScriptImpl(assembly);
		Script* script = nullptr;

		if (m_scripts.find(_assemblyFilename) == m_scripts.end()) // new script
		{
			script = new Script(scriptImple);
			m_scripts.insert(std::make_pair(_assemblyFilename, script));
		}
		else  // existing script reloaded
		{
			script = m_scripts[_assemblyFilename];
			script->ReloadScript(scriptImple);
		}

		return script;
	}
}