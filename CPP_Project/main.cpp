#include "EntryPoint.h"

// be sure to set the Target Directory to the directory of the executable.
// Project Properties -> Debugging -> Working Directory: $(TargetDir)

int main(int argc, const char* argv[])
{
	return Run();
}