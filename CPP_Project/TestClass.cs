﻿using System;
using CLI;

namespace CSharp_Project
{
    public class TestClass
    {
        public static void TestFunction()
        {
            Console.WriteLine("Hello World!");
        }

        public static void TestExposed(ExposedClass e)
        {
            e.cpp_func();
        }
    }
}
