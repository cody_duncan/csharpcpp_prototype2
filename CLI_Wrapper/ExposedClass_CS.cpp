#include "ExposedClass_CS.h"
#include "ExposedClass.h"

using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Collections::Generic;
using namespace System::Reflection;

namespace CLI
{
	ExposedClass::ExposedClass() :
		isWrapper(false),
		internalClass(new Engine::ExposedClass)
	{
	}

	ExposedClass::ExposedClass(Engine::ExposedClass* pExposedClass) :
		isWrapper(true),
		internalClass(pExposedClass)
	{}

	int ExposedClass::cpp_int::get()
	{
		return internalClass->cpp_int;
	}

	void ExposedClass::cpp_int::set(int value)
	{
		internalClass->cpp_int = value;
	}

	ExposedClass::~ExposedClass()
	{
		if(!isWrapper)
		{
			delete internalClass;
		}
	}

	void ExposedClass::cpp_func()
	{
		internalClass->cpp_func();
	}
}
	