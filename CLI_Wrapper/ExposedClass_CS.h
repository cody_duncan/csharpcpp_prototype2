#include <vcclr.h>

namespace Engine
{
	class ExposedClass;
}
#pragma make_public (Engine::ExposedClass)


namespace CLI
{
	public ref class ExposedClass
	{
	public:
		ExposedClass();
		ExposedClass(Engine::ExposedClass*);

		~ExposedClass();

		property int cpp_int
		{
			int get();
			void set(int value);
		}
		
		void cpp_func();
	internal:
		Engine::ExposedClass* internalClass;

	private:
		bool isWrapper;
	};
}