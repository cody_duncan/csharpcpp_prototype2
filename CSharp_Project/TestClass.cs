﻿using System;
using CLI;

namespace CSharp_Project
{
    public class TestClass
    {
        static ExposedClass eClass = new ExposedClass();

        public static void TestFunction()
        {
            //test
            Console.WriteLine("Hello World!");

            //test assignment to engine
            eClass.cpp_int = 17;

            //test call to engine function
            EngineFunc.ReceiveExposedFunc(eClass);

            // Hammer the garbage collector with allocations of wrapped objects.
            for(int k = 0; k < 100; ++k)
            {
                ExposedClass[] array = new ExposedClass[10000];
                for (int i = 0; i < 10000; ++i)
                {
                    array[i] = new ExposedClass();
                    array[i].cpp_int += i*i;
                }

                for (int i = 0; i < 10000; ++i)
                {
                    Console.WriteLine("{0}", array[i].cpp_int);
                }
            }
            
        }

        public static void TestExposed(ExposedClass e)
        {
            //test call to engine object member function
            e.cpp_func();
        }
    }
}
